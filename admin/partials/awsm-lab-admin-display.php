<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       jordiradstake.nl
 * @since      0.1
 *
 * @package    Awsm_Lab
 * @subpackage Awsm_Lab/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
