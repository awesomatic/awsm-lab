<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       jordiradstake.nl
 * @since      0.1
 *
 * @package    Awsm_Lab
 * @subpackage Awsm_Lab/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
