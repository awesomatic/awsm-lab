<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       jordiradstake.nl
 * @since      0.1
 *
 * @package    Awsm_Lab
 * @subpackage Awsm_Lab/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.1
 * @package    Awsm_Lab
 * @subpackage Awsm_Lab/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Lab_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.1
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'awsm-lab',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
