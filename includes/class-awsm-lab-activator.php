<?php

/**
 * Fired during plugin activation
 *
 * @link       jordiradstake.nl
 * @since      0.1
 *
 * @package    Awsm_Lab
 * @subpackage Awsm_Lab/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1
 * @package    Awsm_Lab
 * @subpackage Awsm_Lab/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Lab_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1
	 */
	public static function activate() {

	}

}
